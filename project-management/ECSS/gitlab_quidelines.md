# General Linux4Space code development guidelines

The aim of this document is to briefly describe the software development process (documentation, contributing, testing) in the gitlab.com environment to meet the requirements of the ECSS standards (especially ESCC-E-ST-40C).

## General principles

### Contribution rules

1. Each project has assigned the main maintainer.
2. Each new feture is implemented in separated branch and when finished, it is merged by project maintainer.
#### Commit style
The commit style is loosely inspired by open source projects like Linux, git and openembedded. The format is:

```
area of code: summary

Explanation of the change (why it has been made).
```

The 'area of code' might be something like 'kernel' for a kernel recipe. It is useful when you searching for changes in the respective area. The summary should briefly describe the nature of change (e.g., "add support for command xxx" or "fix incorrect size of yyy"). The explanation line should describe the reason of the change. Try to explain why it has been made. The actual code show what is changed, so you don't need to explain it.

It is useful to add 'Fixes:' tag if you change the bugfix (see [Linux Submitting patches](https://www.kernel.org/doc/html/v5.17/process/submitting-patches.html#describe-your-changes)).

The inspiration (see below for exceptions) may be found in:

- [Linux's Submitting patches](https://www.kernel.org/doc/html/v5.17/process/submitting-patches.html) (sections 'Describe your changes', 'Separate your changes', 'Style-check your changes' and 'The canonical patch format')

- [U-Boot Patches and Feature Requests](https://www.denx.de/wiki/U-Boot/Patches) (section 'Commit message conventions')


The exceptions:
- don't wrap line when you pasting longer error message


### Issue tracking

1. To track the issues and bug fixes the Gitlab *issues* shal be used.
2. For each issue, the impact level must be determined within the following range: critical, high, medium, low, backlog. 
3. The project maintainer shall assign an issue responsible resolver from the Linux4Space development team. 
4. For each issue a new branch shall be created and later merged by the project maintainer when the issue is resolved. 
5. The issue is closed when it is merged to the main branch.
# Linux4Space Project Management
This repository is dedicated to meeting plans, minutes and documents related to meetings. 
The main aim is to keep everyone informed what was discussed on the meetings. 

# Linux4Space meetings
The calls are organized once every 14 days on Wednesdays, at 17:00 CET. The agenda shall be announced here in planned minutes.
The next meeting is planned for 9/27/2023.
If you want to attend the meetings, use the [registration](https://linux4space.org/form/b43d61e1-4160-4013-a30e-a576a299dd4f).

# Project information
Linux4Space is a collaborative open source project founded with the intention of creating an open source Yocto based Linux distribution suitable for space applications. The project brings together all the stakeholders: the software and hardware developers, the suppliers, and technology companies. Linux4Space is designed to be compliant with ESA (European Space Agency) Standards and it is based on community defined requirements.

General project desription is available at its [webpage](https://linux4space.org). 
Detailed information about Linux in space aplications is available in our [wiki](https://wiki.linux4space.org).

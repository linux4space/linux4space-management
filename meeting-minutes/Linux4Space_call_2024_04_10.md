# Planned agenda 2024-04-10

Peter presented Cubespace space procotol implementation for L4S.

# Meeting minutes
## Attending:
Lukáš Mázl, TUL CZ
Lenka Kosková Třísková, TUL CZ
Peter Spurny, TUL CZ
Eric Weiss

1. Lukas fixed pipelines for linux4space Yocto and linux4space-meta. The pipelines provides smokes test when a new commit comes.

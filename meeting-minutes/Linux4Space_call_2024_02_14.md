# Planned agenda 2024-02-14

1. Next development phase

# Meeting minutes
## Attending:
Lenka Kosková Třísková, TUL CZ
Lukáš Mázl, TUL CZ
Tomáš Novotný, VZLU CZ

1. Lukas will fix the building pipelines. The pipeline will generate core-minimal-image for qemu.
2. Lukas will setup Linux4Space distro conf
3. Sun sensor made by TUL student will be present at the next meeting (28.2.).

# Planned agenda 2024-03-27

Presented GitLab changes.

# Meeting minutes
## Attending:
Lukáš Mázl, TUL CZ
Tomáš Novotný, VZLU CZ

1. Export of L4S SW requirements into Gitlab as issues.
2. Remove the meta-linux4space-core into separated repository from Linux4Space-Yocto
3. Prepare pipeline for meta-linux4space
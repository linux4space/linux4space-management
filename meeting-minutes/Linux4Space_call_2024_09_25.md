# Planned agenda 2024-05-08

Telemetry and diagnostic service, file system testing.


# Meeting minutes
Lukas will fix libcsp recipes based on Tomas`s review. At the next meeting, we should discuss how to test requirements related to the File System.

## Attending:

Lukáš Mázl, TUL CZ
Tomáš Novotný, VZLU CZ
# Planned agenda 2024-02-28

1. Next development phase

# Meeting minutes
## Attending:
Lenka Kosková Třísková, TUL CZ
Lukáš Mázl, TUL CZ
Anastazie Rosova, TUL CZ
Tomáš Novotný, VZLU CZ
David Hriadel, VZLU CZ
Krzysztof Garczyński

1. Anastazie presented her diploma work aimed at the    implementation of ADCS.
2. Lukas will prepare gitlab pipeple, which will use sstate cache. This will increase a speed of kernel compiling.
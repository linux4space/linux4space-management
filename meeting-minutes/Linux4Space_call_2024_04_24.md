# Planned agenda 2024-04-24

Telemetry and diagnostic service

Presentation is avaible here: [L4S Telemetry and diagnostic service](https://docs.google.com/presentation/d/1Mn2VY96OU6YPpmUyDsFCOoCDajrvz3moTvWHUV4k0Ws)

# Meeting minutes
## Attending:

Lukáš Mázl, TUL CZ
Lenka Kosková Třísková, TUL CZ
Tomáš Novotný, VZLU CZ
Patric Oppel, Sydney University

1. Lukas presented first propose and steps, which will help to implement telemetry data collection services.
2. Participants agreed that system shall be configurable and indemendat on other modules (telemetry module). Also the system shall have a simple API, which other apps can use.

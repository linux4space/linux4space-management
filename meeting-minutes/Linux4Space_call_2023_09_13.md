# Planned agenda 2023-09-13

1. The current state of the Gitlab repository (Lucas)
2. ECSS and development (Lucas) [presentation](https://docs.google.com/presentation/d/1tU_frOFvrfIOHybl6eCStaeTvz19RSuFILRtSmvSaqc/edit#slide=id.p)
3. Meeting minutes on gitlab (LenkaKT)
4. A brief description mapping ECSS development requirents to our development process. Commit style reference? (LenkaKT)

# Meeging minutes
Attending: Lenka Koskova Triskova (TUL CZ), Lukas Mazl (TUL CZ), Tomas Novotny (VZLU CZ), David VomLehn (Astra)

 1. mailing list - short info by Lucas - TUL does not allow to run SMTP which is needed for mailing list. Lenka will help to solve it.
 2. The current state of the Gitlab repository: problem with merging into Linux4Space yocto layer - pipeline fails, probably not working Erics computer dedicated to computation, Lenka will fix it with help of TUL.
 3. ECSS and development: the document proposal is available in Linux4Space management project at gitlab. Agreed by the meeting attendees. Contribution rules sent by Tomas, will be added to document by Lenka.
 4. ESA support: TUL has renewed the netotiation with Czech ESA ambassador, more information will come next weeks.
